
def content():
    TAG_DICT = {"Football Leagues": [
                    ["Premier League", "Premier League"], ["La Liga", "La Liga"], ["Serie A", "Serie A"],
                    ["Bundesliga", "Bundesliga"], ["Ligue 1", "Ligue 1"]
                ],
                "Competitions" : [
                    ["Champions League", "UEFA Champions League"], ["Europa League", "UEFA Europa League"],
                    ["FA Cup", "FA Cup"], ["Copa del Rey", "Copa Del Rey"], ["Coupe de France", "Coupe de France"],
                    ["Coppa Italia", "Coppa Italia"], ["DFB", "DFB Pokal"]
                ]
                }
    return TAG_DICT


def league():
    leagues = ["Premier League", "FA Cup", "La Liga", "Copa del Rey", "Bundesliga", "Ligue 1", "Serie A",
               "Coppa Italia",
               "CAF", "Taça de Portugal", "Turkish Cup", "Africa Cup of Nations", "Club Friendlies", "Portugal Cup",
               "Primeira Liga", "Eredivisie", "Super Lig", "International friendlies", "Championship",
               "Coupe de la Ligue",
               "EFL Cup", "Coupe de France", "EFL Championship", "Super Lig", "Scottish Premiership",
               "Italian Super Cup",
               "Club World Cup", "Europa League", "Champions League", "UEFA", "World Cup", "Qualifications"]
    return leagues


def user_agent():
    agent = [
                "Highlights", "Soccer", "Reddit Scraper", "reddit", "Flask", "Python", "Agent", "streams", "links",
                "test", "football highlights", "soccerstreams", "Jinja", "random words", "beautiful", "soup", "parsing"
            ]
    return agent

