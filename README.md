### Download the source code for this project ###
[Documentation and code examples](http://bit.ly/2jdn8sF)

This web app was inspired by my previous [CLI project for the EURO 2016](https://github.com/jctissier/Euro2016_TerminalApp)

### :soccer: FootyLinks :soccer: ###
FootyLinks is a Python/Flask web app that I created for fun which helps you find:

* Live football games scores & details
* Links for any live match streams
* Match highlights
* All kinds of stats


No logins or emails required, and there will **never** be Ads. 

This is purely for fixing those football cravings without the headache of clicking through multiple websites.😀

## Demo ##

### Finding Live Match Streams ###
![Demo LiveStream](http://i.imgur.com/Um4DIL0.gif)

### Finding Match Highlights ###
![Demo LiveStream](http://i.imgur.com/xsZjoMH.gif)

### Final Notes ###
Feel free to comment, request new features or contribute,I would love to hear some feedback. 😀